# COM 450

## Trabajo (biblioteca)

### Integrantes gitlab:

- (Product Owner) Ing. Carlos Montellano: [@cmontellano](https://gitlab.com/cmontellano)
- (Developer) Jose Gael: [@gaelelpapi](https://gitlab.com/gaelelpapi)
- (Developer) Gary Guzmán: [@garyDav](https://gitlab.com/garyDav)

## Pasos:

- Requerimientos escritos
- backlog priorizado
- puntos de historia
- esfuerzo
- prioridades
- tecnología

### Requerimientos:

Necesitamos guardar los préstamos de los libros con los siguientes datos:

- libro (nombre, autor, editorial, fecha_lanzamiento)
- usuario (nombre, correo, contraseña): "quién prestó"
- cliente (nombre, celular): "a quién prestó"
- alquiler (usuario, cliente, libro, fecha_prestamo, dias_prestamo): "cantidad de días de préstamo"

>Para la devolución, antomamos la fecha cuando se devolvió el libro

#### Reporte:

- Qué libros se encuentran prestados y cuales no.

- Cuáles son los libros más requeridos.

- Aumentar libros y dar de baja.

- Clientes que no devolvieron a tiempo y que no devolvieron.

### Backlog


